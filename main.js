const readline = require('node:readline');

const CELL_CHAR = 'x'
const WALL_CHAR = ' '

const MIN_FIELD_SIZE = 2
const MAX_FIELD_SIZE = 10

const FREE_CELL = 0
const FIGURE_CELL = 1
const WALL_CELL = 2
const FILLED_CELL = 3

const PRINT_VARIANTS = false

const figures = [
    [
        [1, 1, 0, 0],
        [1, 1, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 0, 0, 0],
        [1, 0, 0, 0],
        [1, 0, 0, 0],
        [1, 0, 0, 0],
    ],
    [
        [1, 1, 1, 1],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 1, 0, 0],
        [0, 1, 0, 0],
        [1, 1, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 0, 0, 0],
        [1, 1, 1, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 0, 0],
        [1, 0, 0, 0],
        [1, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 0, 0, 0],
        [1, 0, 0, 0],
        [1, 1, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 1, 0],
        [1, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 0, 1, 0],
        [1, 1, 1, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 0, 0],
        [0, 1, 1, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 1, 0, 0],
        [1, 1, 0, 0],
        [1, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 1, 1, 0],
        [1, 1, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 0, 0, 0],
        [1, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 1, 0, 0],
        [1, 1, 1, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 0, 0, 0],
        [1, 1, 0, 0],
        [1, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 1, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 1, 0, 0],
        [1, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 0],
    ],
]

const figures_chars = ['O', 'I', 'i', 'J', 'j', 'J', 'j', 'L', 'l', 'L', 'l', 'Z', 'z', 'S', 's', 'T', 't', 'T', 't']

/**
 * Recursive function to calculate the size of the free area starting from a given point.
 *
 * @param {number} x - x-coordinate of the point to start from.
 * @param {number} y - y-coordinate of the point to start from.
 * @param {Array<Array<number>>} field_state - 2D array representing the state of the field.
 * @return {number} size of the free area starting from the given point.
 */
function get_free_area_recursively(x, y, field_state) {
    field_state[y][x] = FILLED_CELL
    let count = 1
    // up
    if (y > 0 && field_state[y - 1][x] == FREE_CELL)
        count += get_free_area_recursively(x, y - 1, field_state)
    // down
    if (y < field_state.length - 1 && field_state[y + 1][x] == FREE_CELL)
        count += get_free_area_recursively(x, y + 1, field_state)
    // left
    if (x > 0 && field_state[y][x - 1] == FREE_CELL)
        count += get_free_area_recursively(x - 1, y, field_state)
    // right
    if (x < field_state[y].length - 1 && field_state[y][x + 1] == FREE_CELL)
        count += get_free_area_recursively(x + 1, y, field_state)
    return count
}

/**
 * Cleans the temporary filled cells in the field.
 *
 * @param {Array<Array<number>>} field_state - 2D array representing the state of the field.
 * @param {number} w - width of the field
 * @param {number} h - height of the field
 * @return {void}
 */
function clean_temp_fill(field_state, w, h) {
    for (let i_y = 0; i_y < h; ++i_y) {
        for (let i_x = 0; i_x < w; ++i_x) {
            if (field_state[i_y][i_x] == FILLED_CELL) field_state[i_y][i_x] = FREE_CELL
        }
    }
}

/**
 * Check if the field has space multiple of 4.
 *
 * @param {Array<Array<number>>} field_state - 2D array representing the state of the field.
 * @param {number} w - width of the field
 * @param {number} h - height of the field
 * @return {boolean} true if the field has space multiple of 4, false otherwise
 */
function check_tetra_space(field_state, w, h) {
    for (let i_y = 0; i_y < h; ++i_y) {
        for (let i_x = 0; i_x < w; ++i_x) {
            if (field_state[i_y][i_x] != FREE_CELL) continue
            if (get_free_area_recursively(i_x, i_y, field_state, w, h) % 4 > 0) {
                clean_temp_fill(field_state, w, h)
                return false
            }
        }
    }
    clean_temp_fill(field_state, w, h)
    return true
}

/**
 * Check if a figure can be placed on the field based on the given position and field state.
 *
 * @param {number} fig_index - index of the figure to be placed
 * @param {number} x - x-coordinate of the position to place the figure
 * @param {number} y - y-coordinate of the position to place the figure
 * @param {Array<Array<number>>} field_state - 2D array representing the state of the field.
 * @param {number} w - width of the field
 * @param {number} h - height of the field
 * @return {boolean} true if the figure can be placed, false otherwise
 */
function can_place_figure(fig_index, x, y, field_state, w, h) {
    let adjacent = false;
    for (let i_fy = 0; i_fy < 4; ++i_fy) {
        for (let i_fx = 0; i_fx < 4; ++i_fx) {
            if (figures[fig_index][i_fy][i_fx] == 0) continue;
            const real_y = y + i_fy;
            const real_x = x + i_fx;
            if (real_y >= h) return false
            if (real_x >= w) return false
            if (field_state[real_y][real_x] > 0) return false
            if (!adjacent) {
                // any point adjacent with border or filled cell
                if (real_y == 0 || real_x == 0) {
                    adjacent = true
                    continue
                }
                if (real_y == h - 1 || real_x == w - 1) {
                    adjacent = true
                    continue
                }
                if (field_state[real_y - 1][real_x] != FREE_CELL ||
                    field_state[real_y][real_x - 1] != FREE_CELL) {
                    adjacent = true
                }
            }
        }
    }
    return adjacent;
}

/**
 * Places a figure on the field state at the specified position.
 *
 * @param {number} fig_index - index of the figure to be placed
 * @param {number} x - x-coordinate of the position
 * @param {number} y - y-coordinate of the position
 * @param {Array<Array<number>>} field_state - 2D array representing the state of the field.
 * @param {number} value - value to be placed on the field state
 * @return {void}
 */
function place_figure(fig_index, x, y, field_state, value) {
    for (let i_fy = 0; i_fy < 4; ++i_fy) {
        for (let i_fx = 0; i_fx < 4; ++i_fx) {
            if (figures[fig_index][i_fy][i_fx] == 0) continue
            field_state[y + i_fy][x + i_fx] = value
        }
    }
}

/**
 * Recursively builds a tree structure representing possible figure placements on a field.
 *
 * @param {Object} parent - parent node of the tree.
 * @param {number} x - x-coordinate for placing the figure.
 * @param {number} y - y-coordinate for placing the figure.
 * @param {Array<Array<number>>} field_state - 2D array representing the state of the field.
 * @param {number} w - width of the field grid.
 * @param {number} h - height of the field grid.
 * @return {void}
 */
function build_tree_recursively(parent, x, y, field_state, w, h) {
    for (let fig_index = 0; fig_index < 19; ++fig_index) {
        if (can_place_figure(fig_index, x, y, field_state, w, h)) {
            place_figure(fig_index, x, y, field_state, FIGURE_CELL)
            if (check_tetra_space(field_state, w, h)) {
                // create and add node
                parent.children.push({
                    figure_index: fig_index,
                    x: x,
                    y: y,
                    children: [],
                })
                // find next step
                for (let i_fy = 0; i_fy < h; ++i_fy) {
                    for (let i_fx = 0; i_fx < w; ++i_fx) {
                        build_tree_recursively(parent.children[parent.children.length - 1], i_fx, i_fy, field_state, w, h)
                    }
                }
            }
            // clean up
            place_figure(fig_index, x, y, field_state, FREE_CELL)
        }
    }
}

/**
 * Calculate the number of free cells in the field state.
 *
 * @param {Array<Array<number>>} field_state - 2D array representing the state of the field.
 * @param {number} w - width of the field
 * @param {number} h - height of the field
 * @param {number} free_value - value indicating a free cell
 * @return {number} count of free cells in the field
 */
function get_free_cells_count(field_state, w, h, free_value) {
    let count = 0;
    for (let i_y = 0; i_y < h; ++i_y) {
        for (let i_x = 0; i_x < w; ++i_x) {
            count += field_state[i_y][i_x] == free_value ? 1 : 0
        }
    }
    return count
}

/**
 * A recursive function to collect tree nodes and their variants.
 *
 * @param {Object} node - current node being processed.
 * @return {Array} Returns a two-dimensional linked list of variants.
 */
function collect_tree_recursively(node) {
    let result = [];
    // This is a leaf - create and return new variant
    if (node.children.length == 0 && node.figure_index >= 0) {
        result.push([{
            figure_index: node.figure_index,
            x: node.x,
            y: node.y,
        }])
        return result
    }
    // Collect all variants and add current node into each of them
    node.children.forEach(function (child) {
        let child_results = collect_tree_recursively(child)
        for (let i = 0; i < child_results.length; ++i) {
            if (node.figure_index >= 0) {
                child_results[i].push({
                    figure_index: node.figure_index,
                    x: node.x,
                    y: node.y,
                })
            }
            result.push(child_results[i])
        }
    })
    return result
}

/**
 * Recursively prints the tree starting from the given node.
 *
 * @param {type} node - starting node to print the tree from
 * @param {type} level - current level of the node in the tree
 * @return {void}
 */
function print_tree_recursively(node, level) {
    for (let i = 0; i < level; ++i) process.stdout.write("\t")
    console.log(`${node.figure_index} at ${node.x}x${node.y}`)
    node.children.forEach(child => {
        print_tree_recursively(child, level + 1)
    })
}


const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});
const prompt = (query) => new Promise((resolve) => rl.question(query, resolve));

(async() => {
    // Input
    let w = parseInt(await prompt(`Enter width (${MIN_FIELD_SIZE}-${MAX_FIELD_SIZE}): `));
    w = Math.max(w, MIN_FIELD_SIZE);
    w = Math.min(w, MAX_FIELD_SIZE);
    console.log(`Width is set to ${w}`);

    let h = parseInt(await prompt(`Enter height (${MIN_FIELD_SIZE}-${MAX_FIELD_SIZE}): `));
    h = Math.max(h, MIN_FIELD_SIZE);
    h = Math.min(h, MAX_FIELD_SIZE);
    console.log(`Height is set to ${h}`);

    let field_data = new Array(h);
    console.log(`Enter field row by row (${CELL_CHAR} for space cell and any another for walls):`);
    for (let i = 0; i < h; ++i) {
        let line = await prompt('');
        field_data[i] = new Array(w);
        for (let j = 0; j < w; ++j) {
            field_data[i][j] = line[j] == CELL_CHAR ? FREE_CELL : WALL_CELL;
        }
    }
    rl.close();

    // Check
    const free_cells_count = get_free_cells_count(field_data, w, h, FREE_CELL)
    if (free_cells_count == 0 || free_cells_count % 4 > 0) {
        console.log('There is no solution');
        return
    }

    mem_before = process.memoryUsage().heapUsed
    // Build tree
    console.log('Processing');
    const start_tick = process.hrtime.bigint()
    let root = {
        figure_index: -1,
        x: 0,
        y: 0,
        children: [],
    }
    for (let i_y = 0; i_y < h; ++i_y) {
        for (let i_x = 0; i_x < w; ++i_x) {
            build_tree_recursively(root, i_x, i_y, field_data, w, h)
            process.stdout.write('.')
        }
        console.log()
    }
    // print_tree_recursively(root, 0);

    // Collect result
    const all_results = collect_tree_recursively(root)
    console.log(`Found variants: ${all_results.length}`)
    const end_tick = process.hrtime.bigint()
    console.log(`--- ${Number(end_tick - start_tick) / 1000000000} seconds ---`);
    mem_after = process.memoryUsage().heapUsed
    console.log(`Memory used: ${(mem_after - mem_before) / 1024 / 1024} MB`)

    // Print result
    let printed_results = []
    all_results.forEach(res_item => {
        let temp_field = new Array(h)
        for (let i = 0; i < h; ++i) {
            temp_field[i] = new Array(w)
            for (let j = 0; j < w; ++j) {
                temp_field[i][j] = field_data[i][j] == FREE_CELL ? CELL_CHAR : WALL_CHAR
            }
        }

        let figures_list = []
        res_item.forEach(f => {
            const figure_char = figures_chars[f.figure_index]
            place_figure(f.figure_index, f.x, f.y, temp_field, figure_char)
            figures_list.push(figure_char.toUpperCase())
        })

        if (get_free_cells_count(temp_field, w, h, CELL_CHAR) > 0) return
        figures_list.sort()
        figures_list = figures_list.join('')
        if (printed_results.includes(figures_list)) return

        if (PRINT_VARIANTS) {
            process.stdout.write('Set: ')
            res_item.forEach(f => {
                const figure_char = figures_chars[f.figure_index]
                process.stdout.write(`${figure_char} `)
            })
            console.log()
            for (let i = 0; i < h; ++i) {
                console.log(temp_field[i].join(''))
            }
        }
        printed_results.push(figures_list)
    })

    console.log('Unique results:', printed_results.length)
})();
