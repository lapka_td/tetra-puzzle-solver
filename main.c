// clang main.c --output bin/c -O3

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

const int MIN_FIELD_SIZE = 2;
const int MAX_FIELD_SIZE = 10;
const int SET_SIZE = (MAX_FIELD_SIZE * MAX_FIELD_SIZE) / 4;

const int FREE_CELL = 0;
const int FIGURE_CELL = 1;
const int WALL_CELL = 2;
const int FILLED_CELL = 3;

const int PRINT_VARIANTS = 0;

const int figures[19][4][4] = {
    {
        {1, 1, 0, 0},
        {1, 1, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 0, 0, 0},
        {1, 0, 0, 0},
        {1, 0, 0, 0},
        {1, 0, 0, 0},
    },
    {
        {1, 1, 1, 1},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 1, 0, 0},
        {0, 1, 0, 0},
        {1, 1, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 0, 0, 0},
        {1, 1, 1, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 0, 0},
        {1, 0, 0, 0},
        {1, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 1, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 0, 0, 0},
        {1, 0, 0, 0},
        {1, 1, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 1, 0},
        {1, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 0, 0},
        {0, 1, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 0, 1, 0},
        {1, 1, 1, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 0, 0},
        {0, 1, 1, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 1, 0, 0},
        {1, 1, 0, 0},
        {1, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 1, 1, 0},
        {1, 1, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 0, 0, 0},
        {1, 1, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 1, 0, 0},
        {1, 1, 1, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 0, 0, 0},
        {1, 1, 0, 0},
        {1, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 1, 0},
        {0, 1, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 1, 0, 0},
        {1, 1, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 0, 0},
    },
};

const char figures_chars[19] = {
    'O', 'I', 'i',
    'J', 'j', 'J', 'j',
    'L', 'l', 'L', 'l',
    'Z', 'z', 'S', 's',
    'T', 't', 'T', 't',
};

size_t total_memory_allocated = 0;
size_t total_allocation_count = 0;
size_t total_memory_freed = 0;
size_t total_free_count = 0;
size_t peak_memory_allocated = 0;
size_t current_memory_allocated = 0;


void *alloc_mem(size_t size) {
    total_memory_allocated += size;
    current_memory_allocated += size;
    ++total_allocation_count;
    if (current_memory_allocated > peak_memory_allocated) {
        peak_memory_allocated = current_memory_allocated;
    }
    return malloc(size);
}

void free_mem(void *ptr, size_t size) {
    total_memory_freed += size;
    current_memory_allocated -= size;
    ++total_free_count;
    free(ptr);
}

void print_mem_usage() {
    if (total_memory_allocated == total_memory_freed &&
        total_allocation_count == total_free_count &&
        current_memory_allocated == 0
    ) {
        printf(
            "Allocated (bytes): %zu, Alloc count: %zu, Peak (bytes): %zu\n",
            total_memory_allocated,
            total_allocation_count,
            peak_memory_allocated
        );
    } else {
        printf(
            "Allocated/Freed (bytes): %zu/%zu, Alloc/Free count: %zu/%zu, Peak (bytes): %zu, Current (bytes): %zd\n",
            total_memory_allocated, total_memory_freed,
            total_allocation_count, total_free_count,
            peak_memory_allocated,
            current_memory_allocated
        );
    }
}

typedef struct TreeNode {
    char figure_index;
    char pos; // x << 4 | y
    struct TreeNode *children;
    struct TreeNode *next;
} TreeNode;

typedef struct ListCol {
    TreeNode *node;
    struct ListCol *next;
} ListCol;

typedef struct ListRow {
    ListCol *head_column;
    struct ListRow *next;
} ListRow;

typedef struct Variant {
    char figures[SET_SIZE];
    struct Variant *next;
} Variant;

int get_free_area_recursively(int x, int y, int **field_state, int w, int h) {
    field_state[y][x] = FILLED_CELL;
    int count = 1;
    // up
    if (y > 0 && field_state[y - 1][x] == FREE_CELL) {
        count += get_free_area_recursively(x, y - 1, field_state, w, h);
    }
    // down
    if (y < h - 1 && field_state[y + 1][x] == FREE_CELL) {
        count += get_free_area_recursively(x, y + 1, field_state, w, h);
    }
    // left
    if (x > 0 && field_state[y][x - 1] == FREE_CELL) {
        count += get_free_area_recursively(x - 1, y, field_state, w, h);
    }
    // right
    if (x < w - 1 && field_state[y][x + 1] == FREE_CELL) {
        count += get_free_area_recursively(x + 1, y, field_state, w, h);
    }
    return count;
}

void clean_temp_fill(int **field_state, int w, int h) {
    for (int i_y = 0; i_y < h; ++i_y) {
        for (int i_x = 0; i_x < w; ++i_x) {
            if (field_state[i_y][i_x] == FILLED_CELL) field_state[i_y][i_x] = FREE_CELL;
        }
    }
}

int check_tetra_space(int **field_state, int w, int h) {
    for (int i_y = 0; i_y < h; ++i_y) {
        for (int i_x = 0; i_x < w; ++i_x) {
            if (field_state[i_y][i_x] != FREE_CELL) continue;
            if (get_free_area_recursively(i_x, i_y, field_state, w, h) % 4 > 0) {
                clean_temp_fill(field_state, w, h);
                return 0;
            }
        }
    }
    clean_temp_fill(field_state, w, h);
    return 1;
}

int can_place_figure(int fig_index, int x, int y, int **field_state, int w, int h) {
    int adjacent = 0;
    for (int i_fy = 0; i_fy < 4; ++i_fy) {
        for (int i_fx = 0; i_fx < 4; ++i_fx) {
            if (figures[fig_index][i_fy][i_fx] == 0) continue;
            int real_y = y + i_fy;
            int real_x = x + i_fx;
            if (real_y >= h) return 0;
            if (real_x >= w) return 0;
            if (field_state[real_y][real_x] > 0) return 0;
            if (!adjacent) {
                // any point adjacent with border or filled cell
                if (real_y == 0 || real_x == 0) {
                    adjacent = 1;
                    continue;
                }
                if (real_y == h - 1 || real_x == w - 1) {
                    adjacent = 1;
                    continue;
                }
                if (field_state[real_y - 1][real_x] != FREE_CELL ||
                    field_state[real_y][real_x - 1] != FREE_CELL) {
                    adjacent = 1;
                }
            }
        }
    }
    return adjacent;
}

void place_figure(int fig_index, int x, int y, int **field_state, int value) {
    for (int i_fy = 0; i_fy < 4; ++i_fy) {
        for (int i_fx = 0; i_fx < 4; ++i_fx) {
            if (figures[fig_index][i_fy][i_fx] == 0) continue;
            field_state[y + i_fy][x + i_fx] = value;
        }
    }
}

void build_tree_recursively(TreeNode *parent, int x, int y, int **field_state, int w, int h) {
    for (int fig_index = 0; fig_index < 19; ++fig_index) {
        if (can_place_figure(fig_index, x, y, field_state, w, h)) {
            place_figure(fig_index, x, y, field_state, FIGURE_CELL);
            if (check_tetra_space(field_state, w, h)) {
                // create and add node
                TreeNode *node = (TreeNode*)alloc_mem(sizeof(TreeNode));
                node->figure_index = (char)fig_index;
                node->pos = (x << 4) | y;
                node->next = NULL;
                node->children = NULL;
                if (!parent->children) {
                    parent->children = node;
                } else {
                    TreeNode *last = parent->children;
                    while (last->next) last = last->next;
                    last->next = node;
                }
                // find next step
                for (int i_fy = 0; i_fy < h; ++i_fy) {
                    for (int i_fx = 0; i_fx < w; ++i_fx) {
                        build_tree_recursively(node, i_fx, i_fy, field_state, w, h);
                    }
                }
            }
            // clean up
            place_figure(fig_index, x, y, field_state, FREE_CELL);
        }
    }
}

int get_free_cells_count(int **field_state, int w, int h, int free_value) {
    int count = 0;
    for (int i_y = 0; i_y < h; ++i_y) {
        for (int i_x = 0; i_x < w; ++i_x) {
            count += field_state[i_y][i_x] == free_value ? 1 : 0;
        }
    }
    return count;
}

ListRow *collect_tree_recursively(TreeNode *node) {
    ListRow *result = NULL;
    // This is a leaf - create and return new variant
    if (node->children == 0 && node->figure_index >= 0) {
        ListCol *variant = (ListCol *)alloc_mem(sizeof(ListCol));
        variant->node = node;
        variant->next = NULL;
        result = (ListRow *)alloc_mem(sizeof(ListRow));
        result->head_column = variant;
        result->next = NULL;
        return result;
    }
    // Collect all variants and add current node into each of them
    TreeNode *child = node->children;
    while (child) {
        ListRow *child_results = collect_tree_recursively(child);
        if (node->figure_index >= 0) {
            ListRow *variant_row = child_results;
            while (variant_row) {
                ListCol *current = (ListCol *)alloc_mem(sizeof(ListCol));
                current->node = node;
                current->next = variant_row->head_column;
                variant_row->head_column = current;
                variant_row = variant_row->next;
            }
        }
        ListRow *last_row = child_results;
        while (last_row->next) last_row = last_row->next;
        last_row->next = result;
        result = child_results;
        child = child->next;
    }
    // Returns two-dimensional linked list
    return result;
}

void print_results(ListRow *results) {
    ListRow *row = results;
    while (row) {
        ListCol *cell = row->head_column;
        while (cell) {
            char ch = figures_chars[(unsigned char)cell->node->figure_index];
            printf("%c-%i ", ch, (int)cell->node->pos);
            cell = cell->next;
        }
        printf("\n");
        row = row->next;
    }
}

void delete_results(ListRow *results) {
    ListRow *row = results;
    while (row) {
        ListCol *cell = row->head_column;
        while (cell) {
            ListCol *tmp_cell = cell;
            cell = cell->next;
            free_mem(tmp_cell, sizeof(ListCol));
        }
        ListRow *tmp_row = row;
        row = row->next;
        free_mem(tmp_row, sizeof(ListRow));
    }
}

void print_tree_recursively(TreeNode *node, int level) {
    for (int i = 0; i < level; ++i) printf("\t");
    printf("%i at %i\n", (int)node->figure_index, (int)node->pos);
    TreeNode *child = node->children;
    while (child) {
        print_tree_recursively(child, level + 1);
        child = child->next;
    }
}

void delete_tree_recursively(TreeNode *node) {
    TreeNode *child = node->children;
    while (child) {
        TreeNode *tmp = child;
        child = child->next;
        delete_tree_recursively(tmp);
    }
    free_mem(node, sizeof(TreeNode));
}

void delete_field(int **field, int w, int h) {
    for (int i = 0; i < h; ++i) free_mem(field[i], sizeof(int) * w);
    free_mem(field, sizeof(int*) * h);
}

int is_unique(Variant *list, char *set) {
    while (list) {
        if (memcmp(list->figures, set, SET_SIZE) == 0) return 0;
        list = list->next;
    }
    return 1;
}

int comp(const void *a, const void *b) {
    char f = *((char*)a);
    char s = *((char*)b);
    if (f > s) return  1;
    if (f < s) return -1;
    return 0;
}

void sort_and_upper(char *inp, char *out) {
    memcpy(out, inp, SET_SIZE);
    int len = 0;
    for (int i = 0; i < SET_SIZE; ++i) {
        if (out[i] > 0) {
            ++len;
            out[i] = toupper(out[i]);
        }
    }
    qsort(out, len, sizeof(char), comp);
}

int main() {
    const char CELL_CHAR = 'x';
    const char WALL_CHAR = '.';

    printf("Enter width (%i-%i): ", MIN_FIELD_SIZE, MAX_FIELD_SIZE);
    int w = 0;
    scanf("%d", &w);
    if (w < MIN_FIELD_SIZE) w = MIN_FIELD_SIZE;
    if (w > MAX_FIELD_SIZE) w = MAX_FIELD_SIZE;
    printf("Width is set to %i\n", w);

    printf("Enter height (%i-%i): ", MIN_FIELD_SIZE, MAX_FIELD_SIZE);
    int h = 0;
    scanf("%d", &h);
    if (h < MIN_FIELD_SIZE) h = MIN_FIELD_SIZE;
    if (h > MAX_FIELD_SIZE) h = MAX_FIELD_SIZE;
    printf("Height is set to %i\n", h);

    int **field_data = (int **)alloc_mem(sizeof(int *) * h);
    printf("Enter field row by row (%c for space cell and %c for walls):\n", CELL_CHAR, WALL_CHAR);
    char buf[256];
    for (int i = 0; i < h; ++i) {
        memset(buf, 0, sizeof(buf));
        scanf("%s", buf);
        int *field_row = (int *)alloc_mem(sizeof(int) * w);
        for (int i_ch = 0; i_ch < w; ++i_ch) {
            field_row[i_ch] = buf[i_ch] == CELL_CHAR ? FREE_CELL : WALL_CELL;
        }
        field_data[i] = field_row;
    }

    // Check
    int free_cells_count = get_free_cells_count(field_data, w, h, FREE_CELL);
    if (free_cells_count == 0 || free_cells_count % 4 > 0) {
        printf("There is no solution\n");
        delete_field(field_data, w, h);
        return 0;
    }

    // Build tree
    printf("Processing\n");
    clock_t begin = clock();
    TreeNode *root = (TreeNode *)alloc_mem(sizeof(TreeNode));
    root->figure_index = -1;
    root->pos = 0;
    root->children = root->next = NULL;
    for (int sy = 0; sy < h; ++sy) {
        for (int sx = 0; sx < w; ++sx) {
            build_tree_recursively(root, sx, sy, field_data, w, h);
            printf(".");
        }
        printf("\n");
    }

    // print_tree_recursively(root, 0);

    // Collect result
    printf("Collecting results\n");
    ListRow *all_results = collect_tree_recursively(root);
    // print_results(all_results);
    int count = 0;
    ListRow *tmp = all_results;
    while (tmp) {
        ++count;
        tmp = tmp->next;
    }
    printf("Found variants: %i\n", count);
    clock_t end = clock();
    double t = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("--- %f seconds ---\n", t);

    // Print result
    Variant *printed_results = NULL;
    ListRow *res_item = all_results;
    while (res_item) {
        // Create temporary field
        int **temp_field = (int **)alloc_mem(sizeof(int *) * h);
        for (int fy = 0; fy < h; ++fy) {
            int *row = (int *)alloc_mem(sizeof(int) * w);
            for (int fx = 0; fx < w; ++fx) {
                row[fx] = field_data[fy][fx] == FREE_CELL ? CELL_CHAR : WALL_CHAR;
            }
            temp_field[fy] = row;
        }

        // Fill temporary field and remember figures set
        ListCol *f = res_item->head_column;
        char figures_list[SET_SIZE];
        memset(figures_list, 0, sizeof(figures_list));
        int pos = 0;
        while (f) {
            TreeNode *elem = f->node;
            char figure_char = figures_chars[(unsigned char)elem->figure_index];
            figures_list[pos] = figure_char;
            int x = elem->pos >> 4;
            int y = elem->pos & 0b1111;
            place_figure((int)elem->figure_index, x, y, temp_field, figure_char);
            f = f->next;
            ++pos;
        }

        if (get_free_cells_count(temp_field, w, h, CELL_CHAR) == 0) {
            // Sort figures_list to figures_set
            char figures_set[SET_SIZE];
            sort_and_upper(figures_list, figures_set);

            if (is_unique(printed_results, figures_set)) {
                // Create new variant
                Variant *v = (Variant *)alloc_mem(sizeof(Variant));
                memcpy(v->figures, figures_set, sizeof(figures_set));
                v->next = printed_results;
                printed_results = v;

                if (PRINT_VARIANTS) {
                    printf("Set: %s.\n", figures_list);
                    for (int fy = 0; fy < h; ++fy) {
                        for (int fx = 0; fx < w; ++fx) printf("%c", (char)temp_field[fy][fx]);
                        printf("\n");
                    }
                }
            }
        }
        delete_field(temp_field, w, h);
        res_item = res_item->next;
    }

    count = 0;
    Variant *tmp_v = printed_results;
    while (tmp_v) {
        ++count;
        tmp_v = tmp_v->next;
    }
    printf("Unique results: %i\n", count);

    while (printed_results) {
        Variant *tmp = printed_results;
        printed_results = printed_results->next;
        free_mem(tmp, sizeof(Variant));
    }

    delete_tree_recursively(root);
    delete_results(all_results);
    delete_field(field_data, w, h);
    print_mem_usage();
    printf("Exit\n");
    return 0;
}
