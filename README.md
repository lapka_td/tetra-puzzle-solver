# Tetra puzzle solver

Solves with brute force the problem of placing tetris pieces on an arbitrary field

## Results for field
xxxxx  
xxxxx  
xxxxx  
xxxxx  
xxxx.

clang version 14.0.0-1ubuntu1.1  
--- 5.731833 seconds ---

java version 11.0.22  
--- 7.863000 seconds ---

odin version dev-2024-03-nightly:4c35633e0  
--- 9.149 seconds ---

zig version 0.12.0-dev.3381+7057bffc1  
--- 9.58553 seconds ---

node.js version 16.14.0  
--- 11.319257195 seconds ---

php version 8.1.2-1ubuntu2.14  
--- 141.53485798836 seconds ---

python version 3.10.12  
--- 576.2767333984375 seconds ---

python version 3.6.0  
--- 1102.621588230133 seconds ---
