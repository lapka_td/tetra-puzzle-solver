// javac main.java -d bin/

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashSet;

class TetraPuzzle {
    static final char CELL_CHAR = 'x';
    static final char WALL_CHAR = ' ';

    static final int MIN_FIELD_SIZE = 2;
    static final int MAX_FIELD_SIZE = 10;

    static final int FREE_CELL = 0;
    static final int FIGURE_CELL = 1;
    static final int WALL_CELL = 2;
    static final int FILLED_CELL = 3;

    static final boolean PRINT_VARIANTS = false;

    static final int[][][] figures = {
        {
            {1, 1, 0, 0},
            {1, 1, 0, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {1, 0, 0, 0},
            {1, 0, 0, 0},
            {1, 0, 0, 0},
            {1, 0, 0, 0},
        },
        {
            {1, 1, 1, 1},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {0, 1, 0, 0},
            {0, 1, 0, 0},
            {1, 1, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {1, 0, 0, 0},
            {1, 1, 1, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {1, 1, 0, 0},
            {1, 0, 0, 0},
            {1, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {1, 1, 1, 0},
            {0, 0, 1, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {1, 0, 0, 0},
            {1, 0, 0, 0},
            {1, 1, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {1, 1, 1, 0},
            {1, 0, 0, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {1, 1, 0, 0},
            {0, 1, 0, 0},
            {0, 1, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {0, 0, 1, 0},
            {1, 1, 1, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {1, 1, 0, 0},
            {0, 1, 1, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {0, 1, 0, 0},
            {1, 1, 0, 0},
            {1, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {0, 1, 1, 0},
            {1, 1, 0, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {1, 0, 0, 0},
            {1, 1, 0, 0},
            {0, 1, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {0, 1, 0, 0},
            {1, 1, 1, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {1, 0, 0, 0},
            {1, 1, 0, 0},
            {1, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {1, 1, 1, 0},
            {0, 1, 0, 0},
            {0, 0, 0, 0},
            {0, 0, 0, 0},
        },
        {
            {0, 1, 0, 0},
            {1, 1, 0, 0},
            {0, 1, 0, 0},
            {0, 0, 0, 0},
        },
    };
    static final char[] figures_chars = {
        'O', 'I', 'i', 'J', 'j', 'J', 'j', 'L', 'l', 'L', 'l', 'Z', 'z', 'S', 's', 'T', 't', 'T', 't',
    };

    static class TreeNode {
        int figure_index;
        int x, y;
        ArrayList<TreeNode> children;
        public TreeNode(int _figure_index, int _x, int _y) {
            figure_index = _figure_index;
            x = _x;
            y = _y;
            children = new ArrayList<>();
        }
    }

    static int get_free_area_recursively(int x, int y, int[][] field_state) {
        field_state[y][x] = FILLED_CELL;
        int count = 1;
        // up
        if (y > 0 && field_state[y - 1][x] == FREE_CELL)
            count += get_free_area_recursively(x, y - 1, field_state);
        // down
        if (y < field_state.length - 1 && field_state[y + 1][x] == FREE_CELL)
            count += get_free_area_recursively(x, y + 1, field_state);
        // left
        if (x > 0 && field_state[y][x - 1] == FREE_CELL)
            count += get_free_area_recursively(x - 1, y, field_state);
        // right
        if (x < field_state[y].length - 1 && field_state[y][x + 1] == FREE_CELL)
            count += get_free_area_recursively(x + 1, y, field_state);
        return count;
    }

    static void clean_temp_fill(int[][] field_state) {
        for (int i_y = 0; i_y < field_state.length; ++i_y)
            for (int i_x = 0; i_x < field_state[i_y].length; ++i_x)
                if (field_state[i_y][i_x] == FILLED_CELL) field_state[i_y][i_x] = FREE_CELL;
    }

    static boolean check_tetra_space(int[][] field_state) {
        for (int i_y = 0; i_y < field_state.length; ++i_y) {
            for (int i_x = 0; i_x < field_state[i_y].length; ++i_x) {
                if (field_state[i_y][i_x] != FREE_CELL)
                    continue;
                int free_count = get_free_area_recursively(i_x, i_y, field_state);
                if (free_count % 4 > 0) {
                    clean_temp_fill(field_state);
                    return false;
                }
            }
        }
        clean_temp_fill(field_state);
        return true;
    }

    static boolean can_place_figure(int fig_index, int x, int y, int[][] field_state) {
        int[][] figure = figures[fig_index];
        boolean adjacent = false;
        int len_y = field_state.length;
        int len_x = field_state[y].length;
        for (int i_fy = 0; i_fy < figure.length; ++i_fy) {
            for (int i_fx = 0; i_fx < figure[i_fy].length; ++i_fx) {
                if (figure[i_fy][i_fx] == 0)
                    continue;
                final int real_y = y + i_fy;
                final int real_x = x + i_fx;
                if (real_y >= len_y)
                    return false;
                if (real_x >= len_x)
                    return false;
                if (field_state[real_y][real_x] > 0)
                    return false;
                if (!adjacent) {
                    // any point adjacent with border or filled cell
                    if (real_y == 0 || real_x == 0) {
                        adjacent = true;
                        continue;
                    }
                    if (real_y == len_y - 1 || real_x == len_x - 1) {
                        adjacent = true;
                        continue;
                    }
                    if (
                        field_state[real_y - 1][real_x] != FREE_CELL
                        || field_state[real_y][real_x - 1] != FREE_CELL
                    )
                        adjacent = true;
                }
            }
        }
        return adjacent;
    }

    static void place_figure(int fig_index, int x, int y, int[][] field_state, int value) {
        int[][] figure = figures[fig_index];
        for (int i_fy = 0; i_fy < figure.length; ++i_fy) {
            for (int i_fx = 0; i_fx < figure[i_fy].length; ++i_fx) {
                if (figure[i_fy][i_fx] == 0)
                    continue;
                field_state[y + i_fy][x + i_fx] = value;
            }
        }
    }

    static void build_tree_recursively(TreeNode parent, int x, int y, int[][] field_state) {
        for (int fig_index = 0; fig_index < figures.length; ++fig_index) {
            if (can_place_figure(fig_index, x, y, field_state)) {
                place_figure(fig_index, x, y, field_state, FIGURE_CELL);
                if (check_tetra_space(field_state)) {
                    // create and add node
                    TreeNode node = new TreeNode(fig_index, x, y);
                    parent.children.add(node);
                    // find next step
                    for (int i_y = 0; i_y < field_state.length; ++i_y)
                        for (int i_x = 0; i_x < field_state[i_y].length; ++i_x)
                            build_tree_recursively(node, i_x, i_y, field_state);
                }
                // clean up
                place_figure(fig_index, x, y, field_state, FREE_CELL);
            }
        }
    }

    static int get_free_cells_count(int[][] field_state, int free_value) {
        int count = 0;
        for (int i_y = 0; i_y < field_state.length; ++i_y)
            for (int i_x = 0; i_x < field_state[i_y].length; ++i_x)
                count += field_state[i_y][i_x] == free_value ? 1 : 0;
        return count;
    }

    static ArrayList<ArrayList<TreeNode>> collect_tree_recursively(TreeNode node) {
        ArrayList<ArrayList<TreeNode>> result = new ArrayList<>();
        if (node.children.size() == 0 && node.figure_index >= 0) {
            ArrayList<TreeNode> variant = new ArrayList<>();
            variant.add(node);
            result.add(variant);
            return result;
        }
        for (TreeNode child_node : node.children) {
            ArrayList<ArrayList<TreeNode>> child_results = collect_tree_recursively(child_node);
            for (ArrayList<TreeNode> i : child_results) {
                if (node.figure_index >= 0)
                    i.add(node);
                result.add(i);
            }
        }
        return result;
    }

    static int ensure_range(int value, int min, int max) {
        return Math.min(Math.max(value, min), max);
    }

    static String string_from_chars(ArrayList<Character> list) {
        StringBuilder builder = new StringBuilder(list.size());
        for(char ch: list) builder.append(ch);
        return builder.toString();
    }

    static String sorted_upper_string(String input) {
        char temp[] = input.toUpperCase().toCharArray();
        Arrays.sort(temp);
        return new String(temp);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println(String.format("Enter width (%d-%d):", MIN_FIELD_SIZE, MAX_FIELD_SIZE));
        final int w = ensure_range(in.nextInt(), MIN_FIELD_SIZE, MAX_FIELD_SIZE);

        System.out.println(String.format("Enter height (%d-%d):", MIN_FIELD_SIZE, MAX_FIELD_SIZE));
        final int h = ensure_range(in.nextInt(), MIN_FIELD_SIZE, MAX_FIELD_SIZE);

        int[][] field_data = new int[h][w];
        System.out.println(String.format("Enter field row by row (%c for space cell and any another for walls):", CELL_CHAR));
        in.nextLine();
        for (int y = 0; y < h; ++y) {
            String line = in.nextLine();
            for (int x = 0; x < w; ++x) {
                final char ch = x >= line.length() ? WALL_CHAR : line.charAt(x);
                field_data[y][x] = ch != CELL_CHAR ? WALL_CELL : FREE_CELL;
            }
        }

        // Check
        final int free_cells_count = get_free_cells_count(field_data, FREE_CELL);
        if (free_cells_count == 0 || free_cells_count % 4 > 0) {
            System.out.println("There is no solution");
            return;
        }

        System.out.println("Processing");
        long start = System.currentTimeMillis();
        TreeNode root = new TreeNode(-1, 0, 0);
        for (int y = 0; y < h; ++y) {
            for (int x = 0; x < w; ++x) {
                build_tree_recursively(root, x, y, field_data);
                System.out.print(".");
            }
            System.out.println("");
        }

        // Collect result
        final ArrayList<ArrayList<TreeNode>> all_results = collect_tree_recursively(root);
        long finish = System.currentTimeMillis();
        System.out.println(String.format("Found variants: %d", all_results.size()));
        System.out.println(String.format("--- %f seconds ---", (finish - start) / 1000.0));

        // Print result
        HashSet<String> printed_results = new HashSet<>();
        for (ArrayList<TreeNode> res_item : all_results) {
            // Create temporary field
            int[][] temp_field = new int[h][w];
            for (int y = 0; y < h; ++y) {
                for (int x = 0; x < w; ++x) {
                    temp_field[y][x] = field_data[y][x] == FREE_CELL ? CELL_CHAR : WALL_CHAR;
                }
            }

            // Fill temporary field
            for (TreeNode n : res_item) {
                final char figure_char = figures_chars[n.figure_index];
                place_figure(n.figure_index, n.x, n.y, temp_field, figure_char);
            }

            if (get_free_cells_count(temp_field, CELL_CHAR) > 0)
                continue;

            // Collect figures from this variant
            ArrayList<Character> figures_list = new ArrayList<>();
            for (TreeNode n : res_item) figures_list.add(figures_chars[n.figure_index]);

            final String key = sorted_upper_string(string_from_chars(figures_list));
            if (printed_results.contains(key))
                continue;

            if (PRINT_VARIANTS) {
                for (char c : figures_list) System.out.print(String.format("%c ", c));
                System.out.println("");
                for (int y = 0; y < h; ++y) {
                    for (int x = 0; x < w; ++x) {
                        System.out.print(String.format("%c", temp_field[y][x]));
                    }
                    System.out.println("");
                }
                System.out.println("");
            }
            printed_results.add(key);
        }

        System.out.println(String.format("Unique results: %d", printed_results.size()));
    }
}
