// zig build-exe main.zig -femit-bin=bin/zig -O ReleaseFast -static -fsingle-threaded -fPIE -fstrip

const std = @import("std");
const stdout = std.io.getStdOut().writer();
const stdin = std.io.getStdIn().reader();

var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const allocator = gpa.allocator();

const MIN_FIELD_SIZE = 2;
const MAX_FIELD_SIZE = 10;
const SET_SIZE = (MAX_FIELD_SIZE * MAX_FIELD_SIZE) / 4;

const FREE_CELL = 0;
const FIGURE_CELL = 1;
const WALL_CELL = 2;
const FILLED_CELL = 3;

const PRINT_VARIANTS = false;

const figures = [19][4][4]i32{
    [4][4]i32{
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 1, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 1, 1, 1 },
        [_]i32{ 0, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 0, 1, 0, 0 },
        [_]i32{ 0, 1, 0, 0 },
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 1, 1, 1, 0 },
        [_]i32{ 0, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 1, 1, 0 },
        [_]i32{ 0, 0, 1, 0 },
        [_]i32{ 0, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 1, 1, 0 },
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 0, 1, 0, 0 },
        [_]i32{ 0, 1, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 0, 0, 1, 0 },
        [_]i32{ 1, 1, 1, 0 },
        [_]i32{ 0, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 0, 1, 1, 0 },
        [_]i32{ 0, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 0, 1, 0, 0 },
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 0, 1, 1, 0 },
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 0, 1, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 0, 1, 0, 0 },
        [_]i32{ 1, 1, 1, 0 },
        [_]i32{ 0, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 1, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 1, 1, 1, 0 },
        [_]i32{ 0, 1, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
    [4][4]i32{
        [_]i32{ 0, 1, 0, 0 },
        [_]i32{ 1, 1, 0, 0 },
        [_]i32{ 0, 1, 0, 0 },
        [_]i32{ 0, 0, 0, 0 },
    },
};

const figures_chars = [19]u8{
    'O', 'I', 'i',
    'J', 'j', 'J',
    'j', 'L', 'l',
    'L', 'l', 'Z',
    'z', 'S', 's',
    'T', 't', 'T',
    't',
};

const TreeNode = struct {
    figure_index: i8,
    x: u8,
    y: u8,
    children: std.ArrayList(*TreeNode),
};

fn get_free_area_recursively(x: usize, y: usize, field_state: *[][]i32, w: usize, h: usize) i32 {
    field_state.*[y][x] = FILLED_CELL;
    var count: i32 = 1;
    // up
    if (y > 0 and field_state.*[y - 1][x] == FREE_CELL) {
        count += get_free_area_recursively(x, y - 1, field_state, w, h);
    }
    // down
    if (y < h - 1 and field_state.*[y + 1][x] == FREE_CELL) {
        count += get_free_area_recursively(x, y + 1, field_state, w, h);
    }
    // left
    if (x > 0 and field_state.*[y][x - 1] == FREE_CELL) {
        count += get_free_area_recursively(x - 1, y, field_state, w, h);
    }
    // right
    if (x < w - 1 and field_state.*[y][x + 1] == FREE_CELL) {
        count += get_free_area_recursively(x + 1, y, field_state, w, h);
    }
    return count;
}

fn clean_temp_fill(field_state: *[][]i32, w: usize, h: usize) void {
    for (0..h) |i_y| {
        for (0..w) |i_x| {
            if (field_state.*[i_y][i_x] == FILLED_CELL) field_state.*[i_y][i_x] = FREE_CELL;
        }
    }
}

fn check_tetra_space(field_state: *[][]i32, w: usize, h: usize) bool {
    for (0..h) |i_y| {
        for (0..w) |i_x| {
            if (field_state.*[i_y][i_x] != FREE_CELL) continue;
            if (@mod(get_free_area_recursively(i_x, i_y, field_state, w, h), 4) > 0) {
                clean_temp_fill(field_state, w, h);
                return false;
            }
        }
    }
    clean_temp_fill(field_state, w, h);
    return true;
}

fn can_place_figure(fig_index: usize, x: usize, y: usize, field_state: *[][]i32, w: usize, h: usize) bool {
    var adjacent = false;
    for (0..4) |i_fy| {
        for (0..4) |i_fx| {
            if (figures[fig_index][i_fy][i_fx] == 0) continue;
            const real_y = y + i_fy;
            const real_x = x + i_fx;
            if (real_y >= h) return false;
            if (real_x >= w) return false;
            if (field_state.*[real_y][real_x] > 0) return false;
            if (!adjacent) {
                // any point adjacent with border or filled cell
                if (real_y == 0 or real_x == 0) {
                    adjacent = true;
                    continue;
                }
                if (real_y == h - 1 or real_x == w - 1) {
                    adjacent = true;
                    continue;
                }
                if (field_state.*[real_y - 1][real_x] != FREE_CELL or
                    field_state.*[real_y][real_x - 1] != FREE_CELL)
                {
                    adjacent = true;
                }
            }
        }
    }
    return adjacent;
}

fn place_figure(fig_index: usize, x: usize, y: usize, field_state: *[][]i32, value: i32) void {
    for (0..4) |i_fy| {
        for (0..4) |i_fx| {
            if (figures[fig_index][i_fy][i_fx] == 0) continue;
            field_state.*[y + i_fy][x + i_fx] = value;
        }
    }
}

fn build_tree_recursively(parent: *TreeNode, x: usize, y: usize, field_state: *[][]i32, w: usize, h: usize) !void {
    for (0..19) |fig_index| {
        if (can_place_figure(fig_index, x, y, field_state, w, h)) {
            place_figure(fig_index, x, y, field_state, FIGURE_CELL);
            if (check_tetra_space(field_state, w, h)) {
                // create and add node
                var node: *TreeNode = try allocator.create(TreeNode);
                node.figure_index = @intCast(fig_index);
                node.x = @intCast(x);
                node.y = @intCast(y);
                node.children = std.ArrayList(*TreeNode).init(allocator);
                try parent.children.append(node);
                // find next step
                for (0..h) |i_fy| {
                    for (0..w) |i_fx| {
                        try build_tree_recursively(node, i_fx, i_fy, field_state, w, h);
                    }
                }
            }
            // clean up
            place_figure(fig_index, x, y, field_state, FREE_CELL);
        }
    }
}

fn get_free_cells_count(field_state: *[][]i32, w: usize, h: usize, free_value: i32) i32 {
    var count: i32 = 0;
    for (0..h) |i_y| {
        for (0..w) |i_x| {
            count += if (field_state.*[i_y][i_x] == free_value) 1 else 0;
        }
    }
    return count;
}

fn collect_tree_recursively(node: *TreeNode) !std.ArrayList(std.ArrayList(*TreeNode)) {
    var result = std.ArrayList(std.ArrayList(*TreeNode)).init(allocator);
    // This is a leaf - create and return new variant
    if (node.children.items.len == 0 and node.figure_index >= 0) {
        var variant = std.ArrayList(*TreeNode).init(allocator);
        try variant.append(node);
        try result.append(variant);
        return result;
    }
    // Collect all variants and add current node into each of them
    for (0..node.children.items.len) |i| {
        var child_results = try collect_tree_recursively(node.children.items[i]);
        for (0..child_results.items.len) |v| {
            if (node.figure_index >= 0) {
                try child_results.items[v].append(node);
            }
            try result.append(child_results.items[v]);
        }
        child_results.deinit();
    }
    // Returns two-dimensional list
    return result;
}

fn print_tree_recursively(node: *TreeNode, level: usize) !void {
    for (0..level) |_| {
        try stdout.print("\t", .{});
    }
    try stdout.print(
        "{} at {}:{} with {} children\n",
        .{ node.figure_index, node.x, node.y, node.children.items.len },
    );
    for (0..node.children.items.len) |i| {
        try print_tree_recursively(node.children.items[i], level + 1);
    }
}

fn delete_tree_recursively(node: *TreeNode) void {
    for (0..node.children.items.len) |i| {
        delete_tree_recursively(node.children.items[i]);
    }
    node.children.deinit();
    allocator.destroy(node);
}

fn ask_int() !usize {
    var buf: [12]u8 = undefined;
    if (try stdin.readUntilDelimiterOrEof(buf[0..], '\n')) |user_input| {
        return std.fmt.parseInt(usize, user_input, 10);
    } else {
        return 0;
    }
}

pub fn main() !void {
    const CELL_CHAR = 'x';
    const WALL_CHAR = '.';

    try stdout.print("Enter width ({}-{}): ", .{ MIN_FIELD_SIZE, MAX_FIELD_SIZE });
    var w: usize = try ask_int();
    w = @max(w, MIN_FIELD_SIZE);
    w = @min(w, MAX_FIELD_SIZE);
    try stdout.print("Width is set to {}\n", .{w});

    try stdout.print("Enter height ({}-{}): ", .{ MIN_FIELD_SIZE, MAX_FIELD_SIZE });
    var h: usize = try ask_int();
    h = @max(h, MIN_FIELD_SIZE);
    h = @min(h, MAX_FIELD_SIZE);
    try stdout.print("Height is set to {}\n", .{h});

    const variant_len = w * h / 4;

    var field_data: [][]i32 = try allocator.alloc([]i32, h);
    try stdout.print(
        "Enter field row by row ({c} for space cell and {c} for walls):\n",
        .{ CELL_CHAR, WALL_CHAR },
    );
    for (0..h) |i| {
        field_data[i] = try allocator.alloc(i32, w);
        var line: [MAX_FIELD_SIZE]u8 = undefined;
        if (try stdin.readUntilDelimiterOrEof(line[0..], '\n')) |user_input| {
            for (0..w) |x| {
                const ch: i32 = if (x >= user_input.len) WALL_CHAR else user_input[x];
                field_data[i][x] = if (ch != CELL_CHAR) WALL_CELL else FREE_CELL;
            }
        } else {
            try stdout.print("Error\n", .{});
            return;
        }
    }

    // Check
    const free_cells_count: i32 = get_free_cells_count(&field_data, w, h, FREE_CELL);
    if (free_cells_count == 0 or @rem(free_cells_count, 4) > 0) {
        try stdout.print("There is no solution\n", .{});
        return;
    }

    // Build tree
    try stdout.print("Processing\n", .{});
    const start_tick: i64 = std.time.microTimestamp();
    var root: *TreeNode = try allocator.create(TreeNode);
    root.figure_index = -1;
    root.x = 0;
    root.y = 0;
    root.children = std.ArrayList(*TreeNode).init(allocator);
    for (0..h) |sy| {
        for (0..w) |sx| {
            try build_tree_recursively(root, sx, sy, &field_data, w, h);
            try stdout.print(".", .{});
        }
        try stdout.print("\n", .{});
    }
    // try print_tree_recursively(&root, 0);

    // Collect result
    const all_results = try collect_tree_recursively(root);
    try stdout.print("Found variants: {}\n", .{all_results.items.len});
    const end_tick: i64 = std.time.microTimestamp();
    const duration: f64 = @as(f64, @floatFromInt(end_tick - start_tick)) / @as(f64, std.time.us_per_s);
    try stdout.print("--- {d} seconds ---\n", .{duration});

    // Print result
    var printed_results = std.StringHashMap(bool).init(allocator);
    for (all_results.items) |res_item| {
        // Init temp field
        var temp_field: [][]i32 = try allocator.alloc([]i32, h);
        for (0..h) |fy| {
            temp_field[fy] = try allocator.alloc(i32, w);
            for (0..w) |fx| {
                temp_field[fy][fx] = if (field_data[fy][fx] == FREE_CELL) CELL_CHAR else WALL_CHAR;
            }
        }

        // Fill temp field
        for (res_item.items) |f| {
            const figure_char = figures_chars[@intCast(f.figure_index)];
            place_figure(@intCast(f.figure_index), f.x, f.y, &temp_field, @intCast(figure_char));
        }

        if (get_free_cells_count(&temp_field, w, h, CELL_CHAR) == 0) {
            var figures_list = try std.ArrayList(u8).initCapacity(allocator, variant_len);
            for (res_item.items) |f| {
                const char = std.ascii.toUpper(figures_chars[@intCast(f.figure_index)]);
                figures_list.appendAssumeCapacity(char);
            }
            std.mem.sort(u8, figures_list.items, {}, comptime std.sort.asc(u8));
            if (printed_results.get(figures_list.items) == null) {
                if (PRINT_VARIANTS) {
                    try stdout.print("Set:", .{});
                    for (res_item.items) |f| {
                        const char = figures_chars[@intCast(f.figure_index)];
                        try stdout.print(" {c}", .{char});
                    }
                    try stdout.print("\n", .{});
                    for (0..h) |fy| {
                        for (0..w) |fx| {
                            try stdout.print("{c} ", .{@as(u8, @intCast(temp_field[fy][fx]))});
                        }
                        try stdout.print("\n", .{});
                    }
                }
                try printed_results.put(figures_list.items, true);
            } else {
                figures_list.deinit();
            }
        }
        for (0..h) |i| {
            allocator.free(temp_field[i]);
        }
        allocator.free(temp_field);
    }

    try stdout.print("Unique results: {}\n", .{printed_results.count()});

    // Cleaning
    for (0..h) |i| {
        allocator.free(field_data[i]);
    }
    allocator.free(field_data);
    delete_tree_recursively(root);
    var iter = printed_results.iterator();
    while (iter.next()) |item| {
        allocator.free(item.key_ptr.*);
    }
    printed_results.deinit();
    for (0..all_results.items.len) |i| {
        all_results.items[i].deinit();
    }
    all_results.deinit();

    try stdout.print("Exit\n", .{});
    // Detecting memory leaks
    std.debug.assert(gpa.deinit() == .ok);
}
