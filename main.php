<?php
declare(strict_types=1);

$CELL_CHAR = 'x';
$WALL_CHAR = ' ';

$MIN_FIELD_SIZE = 2;
$MAX_FIELD_SIZE = 10;

$FREE_CELL = 0;
$FIGURE_CELL = 1;
$WALL_CELL = 2;
$FILLED_CELL = 3;

$PRINT_VARIANTS = false;

$figures = [
    [
        [1, 1, 0, 0],
        [1, 1, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 0, 0, 0],
        [1, 0, 0, 0],
        [1, 0, 0, 0],
        [1, 0, 0, 0],
    ],
    [
        [1, 1, 1, 1],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 1, 0, 0],
        [0, 1, 0, 0],
        [1, 1, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 0, 0, 0],
        [1, 1, 1, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 0, 0],
        [1, 0, 0, 0],
        [1, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 0, 0, 0],
        [1, 0, 0, 0],
        [1, 1, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 1, 0],
        [1, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 0, 1, 0],
        [1, 1, 1, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 0, 0],
        [0, 1, 1, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 1, 0, 0],
        [1, 1, 0, 0],
        [1, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 1, 1, 0],
        [1, 1, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 0, 0, 0],
        [1, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 1, 0, 0],
        [1, 1, 1, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 0, 0, 0],
        [1, 1, 0, 0],
        [1, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [1, 1, 1, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ],
    [
        [0, 1, 0, 0],
        [1, 1, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 0],
    ],
];

$figures_chars = ['O', 'I', 'i', 'J', 'j', 'J', 'j', 'L', 'l', 'L', 'l', 'Z', 'z', 'S', 's', 'T', 't', 'T', 't'];

function get_free_area_recursively(int $x, int $y, array &$field_state, int $w, int $h): int {
    global $FILLED_CELL;
    global $FREE_CELL;
    $field_state[$y][$x] = $FILLED_CELL;
    $count = 1;
    // up
    if ($y > 0 && $field_state[$y - 1][$x] == $FREE_CELL) {
        $count += get_free_area_recursively($x, $y - 1, $field_state, $w, $h);
    }
    // down
    if ($y < $h - 1 && $field_state[$y + 1][$x] == $FREE_CELL) {
        $count += get_free_area_recursively($x, $y + 1, $field_state, $w, $h);
    }
    // left
    if ($x > 0 && $field_state[$y][$x - 1] == $FREE_CELL) {
        $count += get_free_area_recursively($x - 1, $y, $field_state, $w, $h);
    }
    // right
    if ($x < $w - 1 && $field_state[$y][$x + 1] == $FREE_CELL) {
        $count += get_free_area_recursively($x + 1, $y, $field_state, $w, $h);
    }
    return $count;
}

function clean_temp_fill(array &$field_state, int $w, int $h) {
    global $FILLED_CELL;
    global $FREE_CELL;
    for ($i_y = 0; $i_y < $h; ++$i_y) {
        for ($i_x = 0; $i_x < $w; ++$i_x) {
            if ($field_state[$i_y][$i_x] == $FILLED_CELL) $field_state[$i_y][$i_x] = $FREE_CELL;
        }
    }
}

function check_tetra_space(array &$field_state, int $w, int $h): bool {
    global $FREE_CELL;
    for ($i_y = 0; $i_y < $h; ++$i_y) {
        for ($i_x = 0; $i_x < $w; ++$i_x) {
            if ($field_state[$i_y][$i_x] != $FREE_CELL) continue;
            $free_count = get_free_area_recursively($i_x, $i_y, $field_state, $w, $h);
            if ($free_count % 4 > 0) {
                clean_temp_fill($field_state, $w, $h);
                return false;
            }
        }
    }
    clean_temp_fill($field_state, $w, $h);
    return true;
}

function can_place_figure(int $fig_index, int $x, int $y, array &$field_state, int $w, int $h): bool {
    global $figures;
    global $FREE_CELL;
    $adjacent = false;
    for ($i_fy = 0; $i_fy < 4; ++$i_fy) {
        for ($i_fx = 0; $i_fx < 4; ++$i_fx) {
            if ($figures[$fig_index][$i_fy][$i_fx] == 0) continue;
            $real_y = $y + $i_fy;
            $real_x = $x + $i_fx;
            if ($real_y >= $h) return false;
            if ($real_x >= $w) return false;
            if ($field_state[$real_y][$real_x] > 0) return false;
            if (!$adjacent) {
                // any point adjacent with border or filled cell
                if ($real_y == 0 || $real_x == 0) {
                    $adjacent = true;
                    continue;
                }
                if ($real_y == $h - 1 || $real_x == $w - 1) {
                    $adjacent = true;
                    continue;
                }
                if ($field_state[$real_y - 1][$real_x] != $FREE_CELL ||
                    $field_state[$real_y][$real_x - 1] != $FREE_CELL) {
                    $adjacent = true;
                }
            }
        }
    }
    return $adjacent;
}

function place_figure(int $fig_index, int $x, int $y, array &$field_state, $value): void {
    global $figures;
    for ($i_fy = 0; $i_fy < 4; ++$i_fy) {
        for ($i_fx = 0; $i_fx < 4; ++$i_fx) {
            if ($figures[$fig_index][$i_fy][$i_fx] == 0) continue;
            $field_state[$y + $i_fy][$x + $i_fx] = $value;
        }
    }
}

function build_tree_recursively(&$parent, int $x, int $y, array &$field_state, int $w, int $h): void {
    global $FIGURE_CELL;
    global $FREE_CELL;
    for ($fig_index = 0; $fig_index < 19; ++$fig_index) {
        if (can_place_figure($fig_index, $x, $y, $field_state, $w, $h)) {
            place_figure($fig_index, $x, $y, $field_state, $FIGURE_CELL);
            if (check_tetra_space($field_state, $w, $h)) {
                // create and add node
                $parent['children'][] = [
                    'figure_index' => $fig_index,
                    'x' => $x,
                    'y' => $y,
                    'children' => [],
                ];
                // find next step
                for ($i_fy = 0; $i_fy < $h; ++$i_fy) {
                    for ($i_fx = 0; $i_fx < $w; ++$i_fx) {
                        build_tree_recursively($parent['children'][count($parent['children']) - 1], $i_fx, $i_fy, $field_state, $w, $h);
                    }
                }
            }
            // clean up
            place_figure($fig_index, $x, $y, $field_state, $FREE_CELL);
        }
    }
}

function get_free_cells_count(array &$field_state, int $w, int $h, $free_value): int {
    $count = 0;
    for ($i_y = 0; $i_y < $h; ++$i_y) {
        for ($i_x = 0; $i_x < $w; ++$i_x) {
            $count += $field_state[$i_y][$i_x] == $free_value ? 1 : 0;
        }
    }
    return $count;
}

function collect_tree_recursively(&$node): array {
    $result = [];
    // This is a leaf - create and return new variant
    if (count($node['children']) == 0 && $node['figure_index'] >= 0) {
        $result[] = [[
            'figure_index' => $node['figure_index'],
            'x' => $node['x'],
            'y' => $node['y'],
        ]];
        return $result;
    }
    // Collect all variants and add current node into each of them
    for ($i = 0; $i < count($node['children']); ++$i) {
        $variants = collect_tree_recursively($node['children'][$i]);
        foreach ($variants as $variant) {
            if ($node['figure_index'] >= 0) {
                $variant[] = [
                    'figure_index' => $node['figure_index'],
                    'x' => $node['x'],
                    'y' => $node['y'],
                ];
            }
            $result[] = $variant;
        }
    }
    return $result;
}

function print_tree_recursively(&$node, int $level): void {
    for ($i = 0; $i < $level; ++$i) printf("\t");
    printf("{$node['figure_index']} at {$node['x']}x{$node['y']}\n");
    for ($i = 0; $i < count($node['children']); ++$i) {
        print_tree_recursively($node['children'][$i], $level + 1);
    }
}

// Input
$w = intval(readline("Enter width ($MIN_FIELD_SIZE-$MAX_FIELD_SIZE): "));
$w = max($w, $MIN_FIELD_SIZE);
$w = min($w, $MAX_FIELD_SIZE);
printf("Width is set to $w\n");

$h = intval(readline("Enter height ($MIN_FIELD_SIZE-$MAX_FIELD_SIZE): "));
$h = max($h, $MIN_FIELD_SIZE);
$h = min($h, $MAX_FIELD_SIZE);
printf("Height is set to $h\n");

printf("Enter field row by row ($CELL_CHAR for space cell and any another for walls):\n");
$field_data = [];
for ($i_y = 0; $i_y < $h; ++$i_y) {
    $line = readline();
    $field_data[$i_y] = [];
    for ($i_x = 0; $i_x < $w; ++$i_x) {
        $field_data[$i_y][] = $line[$i_x] == $CELL_CHAR ? $FREE_CELL : $WALL_CELL;
    }
}

// Check
$free_cells_count = get_free_cells_count($field_data, $w, $h, $FREE_CELL);
if ($free_cells_count == 0 || $free_cells_count % 4 > 0) {
    printf("There is no solution\n");
    exit(0);
}

// Build tree
printf("Processing\n");
$start_time = microtime(true);
$root = [
    'figure_index' => -1,
    'x' => 0,
    'y' => 0,
    'children' => [],
];
for ($i_y = 0; $i_y < $h; ++$i_y) {
    for ($i_x = 0; $i_x < $w; ++$i_x) {
        build_tree_recursively($root, $i_x, $i_y, $field_data, $w, $h);
        printf('.');
    }
    printf("\n");
}
// print_tree_recursively($root, 0);

// Collect result
$all_results = collect_tree_recursively($root);
$count = count($all_results);
printf("Found variants: $count\n");
$end_time = microtime(true);
printf("--- " . ($end_time - $start_time) . " seconds ---\n");

// Print result
$printed_results = [];
foreach ($all_results as $res_item) {
    // Init temp field
    $temp_field = [];
    for ($i_y = 0; $i_y < $h; ++$i_y) {
        $row = [];
        for ($i_x = 0; $i_x < $w; ++$i_x) {
            $row[] = $field_data[$i_y][$i_x] == $FREE_CELL ? $CELL_CHAR : $WALL_CHAR;
        }
        $temp_field[] = $row;
    }

    // Fill temp field
    foreach ($res_item as $f) {
        $figure_char = $figures_chars[$f['figure_index']];
        place_figure($f['figure_index'], $f['x'], $f['y'], $temp_field, $figure_char);
    }

    if (get_free_cells_count($temp_field, $w, $h, $CELL_CHAR) > 0) {
        continue;
    }

    $figures_list = [];
    foreach ($res_item as $f) {
        $figures_list[] = strtoupper($figures_chars[$f['figure_index']]);
    }
    sort($figures_list);
    $figures_list = implode('', $figures_list);
    if (in_array($figures_list, $printed_results)) {
        continue;
    }

    if ($PRINT_VARIANTS) {
        $result = 'Set:';
        foreach ($res_item as $f) {
            $result .= ' ' . $figures_chars[$f['figure_index']];
        }
        $result .= "\n";
        for ($i_y = 0; $i_y < $h; ++$i_y) {
            for ($i_x = 0; $i_x < $w; ++$i_x) {
                $result .= $temp_field[$i_y][$i_x];
            }
            $result .= "\n";
        }
        printf($result . "\n");
    }
    $printed_results[] = $figures_list;
}

printf("Unique results: " . count($printed_results) . "\n");
