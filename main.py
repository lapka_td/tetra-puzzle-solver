import time
import os
from dataclasses import dataclass, field
import psutil
from dataslots import dataslots


CELL_CHAR = 'x'
WALL_CHAR = ' '

MIN_FIELD_SIZE = 2
MAX_FIELD_SIZE = 10

FREE_CELL = 0
FIGURE_CELL = 1
WALL_CELL = 2
FILLED_CELL = 3

PRINT_VARIANTS = False

figures = [
    [
        [1, 1],
        [1, 1],
    ],
    [
        [1],
        [1],
        [1],
        [1],
    ],
    [
        [1, 1, 1, 1],
    ],
    [
        [0, 1],
        [0, 1],
        [1, 1],
    ],
    [
        [1, 0, 0],
        [1, 1, 1],
    ],
    [
        [1, 1],
        [1, 0],
        [1, 0],
    ],
    [
        [1, 1, 1],
        [0, 0, 1],
    ],
    [
        [1, 0],
        [1, 0],
        [1, 1],
    ],
    [
        [1, 1, 1],
        [1, 0, 0],
    ],
    [
        [1, 1],
        [0, 1],
        [0, 1],
    ],
    [
        [0, 0, 1],
        [1, 1, 1],
    ],
    [
        [1, 1, 0],
        [0, 1, 1],
    ],
    [
        [0, 1],
        [1, 1],
        [1, 0],
    ],
    [
        [0, 1, 1],
        [1, 1, 0],
    ],
    [
        [1, 0],
        [1, 1],
        [0, 1],
    ],
    [
        [0, 1, 0],
        [1, 1, 1],
    ],
    [
        [1, 0],
        [1, 1],
        [1, 0],
    ],
    [
        [1, 1, 1],
        [0, 1, 0],
    ],
    [
        [0, 1],
        [1, 1],
        [0, 1],
    ],
]
figures_chars = ['O', 'I', 'i', 'J', 'j', 'J', 'j', 'L', 'l', 'L', 'l', 'Z', 'z', 'S', 's', 'T', 't', 'T', 't']


@dataslots
@dataclass
class TreeNode:
    figure_index: int = -1
    x: int = 0
    y: int = 0
    children: list = field(default_factory=list)


def get_free_area_recursively(x: int, y: int, field_state: list) -> int:
    field_state[y][x] = FILLED_CELL
    count = 1
    # up
    if y > 0 and field_state[y - 1][x] == FREE_CELL:
        count += get_free_area_recursively(x, y - 1, field_state)
    # down
    if y < len(field_state) - 1 and field_state[y + 1][x] == FREE_CELL:
        count += get_free_area_recursively(x, y + 1, field_state)
    # left
    if x > 0 and field_state[y][x - 1] == FREE_CELL:
        count += get_free_area_recursively(x - 1, y, field_state)
    # right
    if x < len(field_state[y]) - 1 and field_state[y][x + 1] == FREE_CELL:
        count += get_free_area_recursively(x + 1, y, field_state)
    return count


def clean_temp_fill(field_state: list) -> None:
    for i_y in range(len(field_state)):
        for i_x in range(len(field_state[i_y])):
            if field_state[i_y][i_x] == FILLED_CELL:
                field_state[i_y][i_x] = FREE_CELL


def check_tetra_space(field_state: list) -> bool:
    for i_y in range(len(field_state)):
        for i_x in range(len(field_state[i_y])):
            if field_state[i_y][i_x] != FREE_CELL:
                continue
            free_count = get_free_area_recursively(i_x, i_y, field_state)
            if free_count % 4:
                clean_temp_fill(field_state)
                return False
    clean_temp_fill(field_state)
    return True


def can_place_figure(fig_index: int, x: int, y: int, field_state: list) -> bool:
    figure = figures[fig_index]
    adjacent = False
    len_y = len(field_state)
    len_x = len(field_state[y])
    for i_fy in range(len(figure)):
        for i_fx in range(len(figure[i_fy])):
            if not figure[i_fy][i_fx]:
                continue
            real_y = y + i_fy
            real_x = x + i_fx
            if real_y >= len_y:
                return False
            if real_x >= len_x:
                return False
            if field_state[real_y][real_x]:
                return False
            if not adjacent:
                # any point adjacent with border or filled cell
                if real_y == 0 or real_x == 0:
                    adjacent = True
                    continue
                if real_y == len_y - 1 or real_x == len_x - 1:
                    adjacent = True
                    continue
                if field_state[real_y - 1][real_x] != FREE_CELL or field_state[real_y][real_x - 1] != FREE_CELL:
                    adjacent = True
    return adjacent


def place_figure(fig_index: int, x: int, y: int, field_state: list, value) -> None:
    figure = figures[fig_index]
    for i_fy in range(len(figure)):
        for i_fx in range(len(figure[i_fy])):
            if not figure[i_fy][i_fx]:
                continue
            field_state[y + i_fy][x + i_fx] = value


def build_tree_recursively(parent: TreeNode, x: int, y: int, field_state: list) -> None:
    for fig_index in range(len(figures)):
        if can_place_figure(fig_index, x, y, field_state):
            place_figure(fig_index, x, y, field_state, FIGURE_CELL)
            if check_tetra_space(field_state):
                # create and add node
                node = TreeNode(fig_index, x, y)
                parent.children.append(node)
                # find next step
                for i_fy in range(len(field_state)):
                    for i_fx in range(len(field_state[i_fy])):
                        build_tree_recursively(node, i_fx, i_fy, field_state)
            # clean up
            place_figure(fig_index, x, y, field_state, FREE_CELL)


def get_free_cells_count(field_state: list, free_value) -> int:
    count = 0
    for i_y in range(len(field_state)):
        for i_x in range(len(field_state[i_y])):
            count += 1 if field_state[i_y][i_x] == free_value else 0
    return count


def collect_tree_recursively(node: TreeNode) -> list:
    result = []
    if not len(node.children) and node.figure_index >= 0:
        result.append([(node.figure_index, node.x, node.y)])
        return result
    for child_node in node.children:
        child_results = collect_tree_recursively(child_node)
        for i in child_results:
            if node.figure_index >= 0:
                i.append((node.figure_index, node.x, node.y))
            result.append(i)
    return result


def get_process_memory():
    process = psutil.Process(os.getpid())
    mem_info = process.memory_info()
    return mem_info.rss


# Input
w: int = int(input(f'Enter width ({MIN_FIELD_SIZE}-{MAX_FIELD_SIZE}):'))
w = max(w, MIN_FIELD_SIZE)
w = min(w, MAX_FIELD_SIZE)
print(f"Width is set to {w}")

h: int = int(input(f'Enter height ({MIN_FIELD_SIZE}-{MAX_FIELD_SIZE}):'))
h = max(h, MIN_FIELD_SIZE)
h = min(h, MAX_FIELD_SIZE)
print(f"Height is set to {h}")

field_data: list = []
print(f'Enter field row by row ({CELL_CHAR} for space cell and any another for walls):')
for _ in range(0, h):
    row = input()
    row_array = []
    for i_ch in range(w):
        ch = WALL_CHAR if i_ch >= len(row) else row[i_ch]
        row_array.append(WALL_CELL if ch != CELL_CHAR else FREE_CELL)
    field_data.append(row_array)

# Check
free_cells_count = get_free_cells_count(field_data, FREE_CELL)
if free_cells_count == 0 or free_cells_count % 4:
    print('There is no solution')
    exit()

mem_before = get_process_memory()
# Build tree
print('Processing')
start_time = time.time()
root = TreeNode(-1, 0, 0)
for sy in range(h):
    for sx in range(w):
        build_tree_recursively(root, sx, sy, field_data)
        print(".", end='')
    print("\n", end='')

# Collect result
all_results = collect_tree_recursively(root)
print("Found variants: " + str(len(all_results)))
print("--- %s seconds ---\n" % (time.time() - start_time))

mem_after = get_process_memory()

print("memory before: {:,}, after: {:,}, consumed: {:,}".format(mem_before, mem_after, mem_after - mem_before))

# Print result
printed_results = set()
for res_item in all_results:
    temp_field = []
    figures_list = []

    for fy in range(len(field_data)):
        temp_field.append([])
        for fx in range(len(field_data[fy])):
            temp_field[fy].append(CELL_CHAR if field_data[fy][fx] == FREE_CELL else WALL_CHAR)

    for f in res_item:
        figure_char = figures_chars[f[0]]
        figures_list.append(figure_char.upper())
        place_figure(f[0], f[1], f[2], temp_field, figure_char)

    if get_free_cells_count(temp_field, CELL_CHAR):
        continue

    figures_list = tuple(sorted(figures_list))
    if figures_list in printed_results:
        continue

    if PRINT_VARIANTS:
        print(figures_list)
        for fy in range(len(temp_field)):
            print(''.join(temp_field[fy]))
        print('')

    printed_results.add(figures_list)

print("Unique results: " + str(len(printed_results)))
