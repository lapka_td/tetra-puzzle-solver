package tetrapuzzle

// odin build main.odin -file -out:bin/odin -o:speed -no-bounds-check

import "core:fmt"
import "core:os"
import "core:strconv"
import "core:time"
import "core:mem"
import "core:strings"
import "core:slice"

FREE_CELL :: 0
FIGURE_CELL :: 1
WALL_CELL :: 2
FILLED_CELL :: 3

PRINT_VARIANTS :: false

figures := [?][4][4]int{
    {
        {1, 1, 0, 0},
        {1, 1, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 0, 0, 0},
        {1, 0, 0, 0},
        {1, 0, 0, 0},
        {1, 0, 0, 0},
    },
    {
        {1, 1, 1, 1},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 1, 0, 0},
        {0, 1, 0, 0},
        {1, 1, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 0, 0, 0},
        {1, 1, 1, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 0, 0},
        {1, 0, 0, 0},
        {1, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 1, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 0, 0, 0},
        {1, 0, 0, 0},
        {1, 1, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 1, 0},
        {1, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 0, 0},
        {0, 1, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 0, 1, 0},
        {1, 1, 1, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 0, 0},
        {0, 1, 1, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 1, 0, 0},
        {1, 1, 0, 0},
        {1, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 1, 1, 0},
        {1, 1, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 0, 0, 0},
        {1, 1, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 1, 0, 0},
        {1, 1, 1, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 0, 0, 0},
        {1, 1, 0, 0},
        {1, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {1, 1, 1, 0},
        {0, 1, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
    },
    {
        {0, 1, 0, 0},
        {1, 1, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 0, 0},
    },
}

TreeNode :: struct {
    figure_index: int,
    x: int,
    y: int,
    children: [dynamic]TreeNode,
}

get_free_area_recursively :: proc(x, y: int, field_state: ^[dynamic][dynamic]int) -> int {
    field_state[y][x] = FILLED_CELL
    count := 1
    // up
    if y > 0 && field_state[y - 1][x] == FREE_CELL {
        count += get_free_area_recursively(x, y - 1, field_state)
    }
    // down
    if y < len(field_state) - 1 && field_state[y + 1][x] == FREE_CELL {
        count += get_free_area_recursively(x, y + 1, field_state)
    }
    // left
    if x > 0 && field_state[y][x - 1] == FREE_CELL {
        count += get_free_area_recursively(x - 1, y, field_state)
    }
    // right
    if x < len(field_state[y]) - 1 && field_state[y][x + 1] == FREE_CELL {
        count += get_free_area_recursively(x + 1, y, field_state)
    }
    return count
}

clean_temp_fill :: proc(field_state: ^[dynamic][dynamic]int) {
    for i_y in 0..<len(field_state) {
        for i_x in 0..<len(field_state[i_y]) {
            if field_state[i_y][i_x] == FILLED_CELL do field_state[i_y][i_x] = FREE_CELL
        }
    }
}

check_tetra_space :: proc(field_state: ^[dynamic][dynamic]int) -> bool {
    for i_y in 0..<len(field_state) {
        for i_x in 0..<len(field_state[i_y]) {
            if field_state[i_y][i_x] != FREE_CELL do continue
            free_count := get_free_area_recursively(i_x, i_y, field_state)
            if free_count % 4 > 0 {
                clean_temp_fill(field_state)
                return false
            }
        }
    }
    clean_temp_fill(field_state)
    return true
}

can_place_figure :: proc(fig_index, x, y: int, field_state: ^[dynamic][dynamic]int) -> bool {
    figure := figures[fig_index]
    adjacent := false
    len_y := len(field_state)
    len_x := len(field_state[y])
    for i_fy in 0..<len(figure) {
        for i_fx in 0..<len(figure[i_fy]) {
            if figure[i_fy][i_fx] == 0 do continue
            real_y := y + i_fy
            real_x := x + i_fx
            if real_y >= len_y do return false
            if real_x >= len_x do return false
            if field_state[real_y][real_x] > 0 do return false
            if !adjacent {
                // any point adjacent with border or filled cell
                if real_y == 0 || real_x == 0 {
                    adjacent = true
                    continue
                }
                if real_y == len_y - 1 || real_x == len_x - 1 {
                    adjacent = true
                    continue
                }
                if (field_state[real_y - 1][real_x] != FREE_CELL ||
                    field_state[real_y][real_x - 1] != FREE_CELL) {
                    adjacent = true
                }
            }
        }
    }
    return adjacent
}

place_figure :: proc(fig_index, x, y: int, field_state: ^[dynamic][dynamic]int, value: int) {
    figure := figures[fig_index]
    for i_fy in 0..<len(figure) {
        for i_fx in 0..<len(figure[i_fy]) {
            if figure[i_fy][i_fx] == 0 do continue
            field_state[y + i_fy][x + i_fx] = value
        }
    }
}

place_figure_rune :: proc(fig_index, x, y: int, field_state: ^[dynamic][dynamic]rune, value: rune) {
    figure := figures[fig_index]
    for i_fy in 0..<len(figure) {
        for i_fx in 0..<len(figure[i_fy]) {
            if figure[i_fy][i_fx] == 0 do continue
            field_state[y + i_fy][x + i_fx] = value
        }
    }
}

build_tree_recursively :: proc(parent: ^TreeNode, x, y: int, field_state: ^[dynamic][dynamic]int) {
    for fig_index in 0..<len(figures) {
        if can_place_figure(fig_index, x, y, field_state) {
            place_figure(fig_index, x, y, field_state, FIGURE_CELL)
            if check_tetra_space(field_state) {
                // create and add node
                append(&parent.children, TreeNode{fig_index, x, y, make([dynamic]TreeNode, 0, 0)})
                // find next step
                for i_fy in 0..<len(field_state) {
                    for i_fx in 0..<len(field_state[i_fy]) {
                        build_tree_recursively(&parent.children[len(parent.children) - 1], i_fx, i_fy, field_state)
                    }
                }
            }
            // clean up
            place_figure(fig_index, x, y, field_state, FREE_CELL)
        }
    }
}

get_free_cells_count :: proc(field_state: ^[dynamic][dynamic]int, free_value: int) -> int {
    count := 0
    for i_y in 0..<len(field_state) {
        for i_x in 0..<len(field_state[i_y]) {
            count += 1 if field_state[i_y][i_x] == free_value else 0
        }
    }
    return count
}

get_free_cells_count_rune :: proc(field_state: ^[dynamic][dynamic]rune, free_value: rune) -> int {
    count := 0
    for i_y in 0..<len(field_state) {
        for i_x in 0..<len(field_state[i_y]) {
            count += 1 if field_state[i_y][i_x] == free_value else 0
        }
    }
    return count
}

collect_tree_recursively :: proc(node: ^TreeNode) -> ^[dynamic][dynamic]^TreeNode {
    result := make([dynamic][dynamic]^TreeNode, 0, 1)
    if len(node.children) == 0 && node.figure_index >= 0 {
        variant := make([dynamic]^TreeNode, 0, 1)
        append(&variant, node)
        append(&result, variant)
        return &result
    }
    for _, i in node.children {
        child_results := collect_tree_recursively(&node.children[i])^
        for _, i in child_results {
            if node.figure_index >= 0 do append(&child_results[i], node)
            append(&result, child_results[i])
        }
        delete(child_results)
    }
    return &result
}

print_tree_recursively :: proc(node: ^TreeNode, level: int) {
    for i in 0..<level do fmt.print("\t")
    fmt.printf("%i at %i:%i with %i children\n", node.figure_index, node.x, node.y, len(node.children))
    for _, i in node.children do print_tree_recursively(&node.children[i], level + 1)
}

delete_tree_recursively :: proc(node: ^TreeNode) {
    for _, i in node.children do delete_tree_recursively(&node.children[i])
    delete(node.children)
}

read_int :: proc() -> int {
    buf: [100]byte
    n, err := os.read(os.stdin, buf[:])
	if err < 0 do return 0
    str := string(buf[:n])
    return strconv.atoi(str)
}

// Case insensitive search in array
in_array :: proc(array: ^[dynamic]string, value: string) -> bool {
    for i in array {
        if strings.equal_fold(i, value) do return true
    }
    return false
}

upper_rune :: proc(char: rune) -> rune {
    if char < 'a' || char > 'z' do return char
    return 'A' + (char - 'a')
}

sort_cmp :: proc(i, j: rune) -> bool {
    return upper_rune(i) < upper_rune(j)
}

main :: proc() {
    track: mem.Tracking_Allocator
    mem.tracking_allocator_init(&track, context.allocator)
    context.allocator = mem.tracking_allocator(&track)

    defer {
        if len(track.allocation_map) > 0 {
            fmt.eprintf("=== %v allocations not freed: ===\n", len(track.allocation_map))
            for _, entry in track.allocation_map {
                fmt.eprintf("- %v bytes @ %v\n", entry.size, entry.location)
            }
        }
        if len(track.bad_free_array) > 0 {
            fmt.eprintf("=== %v incorrect frees: ===\n", len(track.bad_free_array))
            for entry in track.bad_free_array {
                fmt.eprintf("- %p @ %v\n", entry.memory, entry.location)
            }
        }
        if track.total_memory_allocated == track.total_memory_freed &&
            track.total_allocation_count == track.total_free_count &&
            track.current_memory_allocated == 0 {
            fmt.printf(
                "Allocated (bytes): %i, Alloc count: %i, Peak (bytes): %i\n",
                track.total_memory_allocated,
                track.total_allocation_count,
                track.peak_memory_allocated,
            )
        } else {
            fmt.printf(
                "Allocated/Freed (bytes): %i/%i, Alloc/Free count: %i/%i, Peak (bytes): %i, Current (bytes): %i\n",
                track.total_memory_allocated, track.total_memory_freed,
                track.total_allocation_count, track.total_free_count,
                track.peak_memory_allocated,
                track.current_memory_allocated,
            )
        }
        mem.tracking_allocator_destroy(&track)
    }

    CELL_CHAR :: 'x'
    WALL_CHAR :: ' '

    MIN_FIELD_SIZE :: 2
    MAX_FIELD_SIZE :: 10

    MAX_FIGURES :: MAX_FIELD_SIZE * MAX_FIELD_SIZE / 4

    figures_chars := [?]rune{
        'O', 'I', 'i',
        'J', 'j', 'J', 'j',
        'L', 'l', 'L', 'l',
        'Z', 'z', 'S', 's',
        'T', 't', 'T', 't',
    }

    fmt.printf("Enter width (%i-%i): ", MIN_FIELD_SIZE, MAX_FIELD_SIZE)
    w := read_int()
    if w < MIN_FIELD_SIZE do w = MIN_FIELD_SIZE
    if w > MAX_FIELD_SIZE do w = MAX_FIELD_SIZE
    fmt.printf("Width is set to %i\n", w)

    fmt.printf("Enter height (%i-%i): ", MIN_FIELD_SIZE, MAX_FIELD_SIZE)
    h := read_int()
    if h < MIN_FIELD_SIZE do h = MIN_FIELD_SIZE
    if h > MAX_FIELD_SIZE do h = MAX_FIELD_SIZE
    fmt.printf("height is set to %i\n", h)

    field_data := make([dynamic][dynamic]int, 0, h)
    defer delete(field_data)

    fmt.printf("Enter field row by row (%c for space cell and any another for walls):\n", CELL_CHAR)
    for _ in 0..<h {
        buf: [256]byte
        n, err := os.read(os.stdin, buf[:])
        if err < 0 {
            fmt.println("Error")
            return
        }
        row := string(buf[:n])
        row_array : [dynamic]int = make([dynamic]int, 0, w)
        for i_ch in 0..<w {
            ch := WALL_CHAR if i_ch >= len(row) else row[i_ch]
            append(&row_array, WALL_CELL if ch != CELL_CHAR else FREE_CELL)
        }
        append(&field_data, row_array)
    }

    // Check
    free_cells_count := get_free_cells_count(&field_data, FREE_CELL)
    if free_cells_count == 0 || free_cells_count % 4 > 0 {
        fmt.println("There is no solution")
        return
    }

    // Build tree
    fmt.println("Processing")
    start_tick := time.tick_now()
    children := make([dynamic]TreeNode, 0, 1)
    root := TreeNode{-1, 0, 0, children}
    for sy in 0..<h {
        for sx in 0..<w {
            build_tree_recursively(&root, sx, sy, &field_data)
            fmt.print(".")
        }
        fmt.println("")
    }
    // print_tree_recursively(&root, 0);

    // Collect result
    all_results := collect_tree_recursively(&root)^
    fmt.printf("Found variants: %i\n", len(all_results))
    duration := time.tick_since(start_tick)
	t := f32(time.duration_seconds(duration))
    fmt.printf("--- %f seconds ---\n", t)

    // Print result
    printed_results := make([dynamic]string, 0, 1)
    for res_item in all_results {
        // Init temp field
        temp_field := make([dynamic][dynamic]rune, 0, h)
        defer delete(temp_field)
        for fy in 0..<len(field_data) {
            row := make([dynamic]rune, 0, w)
            for fx in 0..<len(field_data[fy]) {
                append(&row, CELL_CHAR if field_data[fy][fx] == FREE_CELL else WALL_CHAR)
            }
            append(&temp_field, row)
        }

        figures_list := make([dynamic]rune, 0, w * h / 4)
        defer delete(figures_list)

        // Fill temp field
        for f in res_item {
            figure_char := figures_chars[f.figure_index]
            append(&figures_list, figure_char)
            place_figure_rune(f.figure_index, f.x, f.y, &temp_field, figure_char)
        }

        if get_free_cells_count_rune(&temp_field, CELL_CHAR) == 0 {
            slice.stable_sort_by(figures_list[:], sort_cmp)
            builder := strings.builder_make()
            for r in figures_list {
                strings.write_rune(&builder, r)
                strings.write_rune(&builder, ' ')
            }
            figures_string := strings.to_string(builder)
            if in_array(&printed_results, figures_string) == false {
                if PRINT_VARIANTS {
                    fmt.printf("Set: %s\n", figures_string)
                    for fy in 0..<len(temp_field) {
                        for fx in 0..<len(temp_field[fy]) do fmt.print(temp_field[fy][fx])
                        fmt.println("")
                    }
                }
                append(&printed_results, strings.clone(figures_string))
            }
            strings.builder_destroy(&builder)
        }
        for fy in 0..<len(temp_field) do delete(temp_field[fy])
    }

    fmt.printf("Unique results: %i\n", len(printed_results))

    for i in 0..<len(field_data) do delete(field_data[i])
    for _, i in printed_results do delete(printed_results[i])
    delete(printed_results)
    delete_tree_recursively(&root)
    for _, i in all_results do delete(all_results[i])
    delete(all_results)

    fmt.println("Exit")
}
